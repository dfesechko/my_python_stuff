from os import system
import time
import random

def lprint(mlist, mpos=0):
    """Print List histogram"""
    lmax = len(mlist) # Length of the List
    llen = len(mlist) # Length of the List
    ipos = 0 # Start position to mark in Spec Char
    i = 0 # Loop counter
    mchar = '    *  ' # Char to print when match
    nchar = '       ' # Char to print when NOT match
    dchar = '-------' # Dash char
    smchar = '    #  ' # Special Char to print when match
    samchar1 = '    ^  ' # Special Char to print when match
    samchar2 = '    |  ' # Special Char to print when match

    while lmax != 0:
        ipos = 0
        for nval in mlist:
            ipos += 1
            if nval >= lmax:
                if mpos == ipos:
                    print(smchar, end='')
                else:
                    print(mchar, end='')
            else:
                print(nchar, end='')

        print('\n')
        lmax -= 1

    for i in range(0, llen):
        print(dchar, end='')

    print('\n')

    for nval in mlist:
        if nval >= 10 and nval < 100:
            print(f"   {nval}  ", end='')
        else:
            print(f"    {nval}  ", end='')

    print('\n')

    if mpos:
        for i in range(1, llen+1):
            if i == mpos:
                print(samchar1, end='')
            else:
                print(nchar, end='')

        print('\n')

#    for i in range(1, llen+1):
#        if i == mpos:
#            print(samchar2, end='')
#        else:
#            print(nchar, end='')
#
#    print('\n')

def lrandgen(low = 1, high = 10, prec = 20):
    """ Generate a list of rundom numbers from Low to High """
    ulist = []

    for k in range(0, high*prec):
        trand = random.randint(low, high)
        if trand not in ulist:
            ulist.append(trand)

    return ulist

def lparamprint(ulist):
    print(f"Len: {len(ulist)}\tMin: {min(ulist)}\tMax: {max(ulist)}")

def lnsort(ulist):
    icount = 0
    scount = 0
    lmax = llen = len(ulist)
    aflag = True
    cflag = False
    
    while lmax and aflag:
        scount = llen-lmax
        for i in range(0, llen-1-scount):
            icount += 1
            if ulist[i] > ulist[i+1]:
                cflag = True
                ulist[i], ulist[i+1] = ulist[i+1], ulist[i]
        
        if cflag:
            cflag = False
        else:
            aflag = False
    
        lmax -= 1
    
    return icount
##### End of func def
