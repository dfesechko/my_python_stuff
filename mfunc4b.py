from mflib1 import lrandgen as lgen, lnsort as lsort, lparamprint as lstat, lprint
from os import system
import time

sstat = []
fllist = []
falist = []
ftlist = []
fcount = 0
system('clear')
nsamples = 200
prec = 20
low = 1
high = 100
t0 = time.time()
for i in range(1, nsamples+1):
    #print(f"Sample {i}:")
    tlist = lgen(low, high, prec)
    ftlist.append(len(tlist))
    #print(tlist)
    if len(tlist) < high:
        fcount += 1
        fllist.append(len(tlist))
        falist.append(sum(tlist)/len(tlist))
        #print(f"Sample {i}:", end='')
        #lstattlist)
    ncount = lsort(tlist)
    sstat.append(ncount)
    #print(f"Sorted in {ncount} steps.")
    #print(tlist)

t1 = time.time()
#print(f"{sstat}\nMin: {min(sstat)}\tMax: {max(sstat)}\tAvg: {sum(sstat)/len(sstat)}")
#print(f"Min: {min(sstat)}\tMax: {max(sstat)}\tAvg: {sum(sstat)/len(sstat)}")
dev = len(ftlist)
if dev == 0:
    dev = 1

print(f"{100-((100*fcount)/nsamples)}% ({nsamples-fcount} of {nsamples}) succeeded\n"
        f"{(100*fcount)/nsamples}% ({fcount} of {nsamples}) failed\n"
        f"Avg length: {sum(ftlist)/dev}\n"
        f"Total time: {t1 -t0} seconds")
