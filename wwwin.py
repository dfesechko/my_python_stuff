from random import choice as win
from time import sleep as pause
from os import system as sys
from sys import stdout as out

def fwin(dwe, podium):
    """ Find the winner """
    bwins = 0
    winner = 'Nobody'

    # input(f"dwe: {dwe}\npodium: {podium}")

    while dwe:
        for name,nwins in dwe.items():
            if nwins > bwins:
                bwins = nwins
                winner = name

        podium[winner] = bwins
        del dwe[winner]

        fwin(dwe, podium)

    #input(f"dwe: {dwe}\npodium: {podium}")
            
def gpoints(we, podium):
    """ Assign points to players """
    points = 3

    for name in podium:
        we[name] += points
        points -= 1

    tmp = we.copy()
    for name in tmp:
        del we[name]
    fwin(tmp, we)

def pwinner(swe, roundd=0):
    place = 1
    if roundd:
        print(f"Round {roundd} results:")
    else:
        print("Tatol score: ")
    for name in swe:
        print(f"{place}. {name}: {swe[name]}")
        place += 1

    print("\n\n")

def mpwinner(we, podium, roundd=0):
    sch = '*'
    tlen = 65
    """
    for val in swe:
        tlen += len(val)
        tlen = int((4*tlen)/len(swe))
    #input(f"{tlen}")
    """
    place = 1
    print("\n\t", end='')
    for i in range(tlen):
        print(sch, end='')
    print(f"\n\t{sch}\t\t\t\t{sch}\t\t\t\t{sch}")
    print(f"\t{sch}\t", end='')
    print(f"Round {roundd} results:", end='')
    print(f"\t{sch}\t", end='')
    print(f"Tatol score:", end='')
    print(f"\t\t{sch}")

    if len(podium.keys()) == 0:
        for name in we:
            podium[name] = 0
    lwe = list(we.keys())
    lpodium = list(podium.keys())
    llen = len(lwe)
    #print(lwe, lpodium, llen)
    #input()

    for i in range(0, llen):
        spod = podium[lpodium[i]]
        swe = we[lwe[i]]
        print(f"\t{sch}\t", end='')
        print(f"{i+1}. {lpodium[i]}: {spod}", end='')
        if spod < 100:
            print(f"\t\t{sch}\t", end='')
        else:
            print(f"\t{sch}\t", end='')
        print(f"{i+1}. {lwe[i]}: {swe}", end='')
        if i+1 == llen:
            print(f"\t\t{sch}", end='')
        else:
            print(f"\t\t{sch}")

    print(f"\n\t{sch}\t\t\t\t{sch}\t\t\t\t{sch}", end='')
    print("\n\t", end='')
    for i in range(tlen):
        print(sch, end='')
    print('\n')


clr = 'clear'
we = {'Dmitry':0, 'Dustin':0, 'Michael':0}
ch = 100*tuple(name for name in we)
i = roundd = 0

#print(we, ch)
#input("")

for roundd in range(1, 11):
    cwe = we.copy()
    podium = {}
    sys(clr)
    mpwinner(we, podium, roundd-1)
    print(f"\nStarting round {roundd} in 1 sec.")
    pause(1)

    for i in range(0, 1_000_000):
        cwe[win(ch)] += 1

    fwin(cwe, podium)
    gpoints(we, podium)
    sys(clr)
    mpwinner(we, podium, roundd)

    print(f"\nStarting round {roundd+1} in 3 sec.", end='') #5 sec.") #, end='')
    for s in range(3, 0, -1):
        pause(1)
        print('.', end='')
        out.flush()
